<?php
require_once('fonctionnement.php');

$_SESSION['player1'] = '';
$_SESSION['player2'] = '';
$_SESSION['couleur'] = "basic.css";
$_SESSION['bouton'] = "light";

// suppression du cookie de session
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

header('Location: index.php');
?>
