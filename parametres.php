<?php
require_once ('fonctionnement.php');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Parameters</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="./resources/css/<?=$_SESSION['couleur']?>">
</head>

<body>
    <header>
        <h2>Bienvenue sur la meilleure bataille navale du siècle</h2>
    </header>
    <div class="theme">
        <form method="POST" action="">
            <select name="theme">
                <option name="blanc">Blanc</option>
                <option name="bleu">Bleu</option>
                <option name="noir">Noir</option>
                <option name="rouge">Rouge</option>

            </select> <br>
            <input type="submit" name="choisir" value="Choisir" class="btn btn-<?=$_SESSION['bouton']?>"><br>
            <a href="index.php" class="btn btn-<?=$_SESSION['bouton']?>">Accueil</a>
        </form>
    </div>
    <footer>
        <div class="flex-container">
            <div>
                <b>Le but du jeu expliqué avec des mots simples </b><br>
                Informations : <br>
                - Vous jouez sur une même plateforme <br>
                - Chacun aura son tour de jeu <br>
                - Votre tour est limité par un timer <br>
                - Le joueur qui ne joue pas devra regarder ailleurs
            </div>
            <div>
                <b> Groupe composé de</b> <br>
                Adrien <br> Bernardo <br> Pablo <br> Thi-kim
            </div>
        </div>
    </footer>
</body>

</html>