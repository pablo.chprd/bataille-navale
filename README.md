# Adrien - Bernardo - Pablo - Thikim

# Bataille navale
Le but du jeu est d’éliminer tous les bateaux de votre adversaire.

## JEU
La bataille navale est conçue pour 2 joueurs, il n’est pas possible de joueur contre un ordinateur.

## UTILISATEUR
À la page d’accueil, il faudra entrer un nom. Il est possible de 
changer le thème du site selon la préférence du joueur et d’ajouter une image de profil. Après avoir rempli les champs nécessaires, il sera possible de jouer.

## FONCTIONNEMENT
Au lancement de la partie, il y aura seulement une plateforme pour les 2 joueurs. Le placement des bateaux se fera chacun son tour et la partie aussi.
C’est-à-dire que, par exemple, lorsque ce sera au tour du J1, le J2 devra se retourner afin de ne pas voir l’écran. Un timer est mis en place pour que les joueurs sachent quand il faudra inverser les rôles.

## FIN
Le joueur gagnant sera affiché avec son image de profil. Il est possible de recommencer une partie ou quitter.

![bataille_navale](resources/img/img_doc_bataillenavale.PNG)
