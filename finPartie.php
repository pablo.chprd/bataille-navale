<?php
session_start();
if(!isset($_SESSION["joueur"])){
    $_SESSION["joueur"] = ["joueur1" => "joueur1", "joueur2" => "joueur2"];
}

$pseudo = "Fabrice";
$btnAction = filter_input(INPUT_POST, "btnAction");

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" type="text/css" href="finPartie.css">
        <title>Fin de partie</title>
    </head>
    <body>
    <h1>Bravo <?=$pseudo?>, vous avez gagné, vous êtes le plus bo !</h1>
    <img src="./img/gagnant.jpg" name="gagnant" alt="image de la personne qui gagne">
        <!--<form method="POST" action="finPartie.php">
            <button name="btnAction" value="menu">Revenir au menu</button>
            <button name="btnAction" value="recommencer">Recommencer</button>
      </form>-->
      <div>
      <a href="resources/index.php">Revenir au menu</a>
</div>
      <div>
      <a href="bateaux.php">Recommencer</a>
</div>
    </body>
</html>