<?php
session_start();

/*----------------------------------------------------------------------------------------*/
// parametres.php

if (!isset($_SESSION['bouton'])) {
    $_SESSION['bouton'] = "light";
}

$theme = filter_input(INPUT_POST, "theme", FILTER_SANITIZE_STRING);
$choisir = filter_input(INPUT_POST, "choisir", FILTER_SANITIZE_STRING);

// choix du thème
if ($choisir) {
    switch ($theme) {
        case "Blanc":
            $_SESSION['couleur'] = "blanc.css";
            $_SESSION['bouton'] = "secondary";
            break;
            
        case "Bleu":
            $_SESSION['couleur'] = "bleu.css";
            $_SESSION['bouton'] = "info";
            break;
        
        case "Noir":
            $_SESSION['couleur'] = "noir.css";
            $_SESSION['bouton'] = "light";
            break;

        case "Rouge":
            $_SESSION['couleur'] = "rouge.css";
            $_SESSION['bouton'] = "danger";
            break;
        
        default:
            echo "couleur inconnue $theme";
            break;
    }
}
/*----------------------------------------------------------------------------------------*/
// index.php

if (!isset($_SESSION['joueur'])) {
    $_SESSION['couleur'] = "blanc.css";
    $_SESSION['bouton'] = "light";
    $_SESSION["joueur"]["nomJoueur1"] = "";
    $_SESSION["joueur"]["nomJoueur2"] = "";
    $_SESSION["joueur"]["imageJoueur1"] = "";
    $_SESSION["joueur"]["imageJoueur2"] = "";
}

if(!isset($_SESSION["joueur"])){
    $_SESSION["joueur"] = [
                            ["nomJoueur1" => "", "imageJoueur1" => ""],
                            ["nomJoueur2" => "", "imageJoueur2" => ""]
                        ]; 
}

    // Joueurs
    $joueur1 = filter_input(INPUT_POST, "joueur1", FILTER_SANITIZE_STRING);
    $joueur2 = filter_input(INPUT_POST, "joueur2", FILTER_SANITIZE_STRING);
   
    // Boutons
    $submit = filter_input(INPUT_POST, "btnSubmit", FILTER_SANITIZE_STRING);
    $imageJ1 =  filter_input(INPUT_POST, "avatarJ1");
    $imageJ2 = filter_input(INPUT_POST, "avatarJ2");
    
    if($imageJ1){
        $_SESSION["joueur"]["imageJoueur1"] = "resources/img/avatar$imageJ1.jpg";
        $avatarJ1 = $_SESSION["joueur"]["imageJoueur1"];
    }

    if($imageJ2){
        $_SESSION["joueur"]["imageJoueur2"] = "resources/img/avatar$imageJ2.jpg";
        $avatarJ2 = $_SESSION["joueur"]["imageJoueur2"];
    }
    // Erreurs
    $erreur = "";

    if($submit){
        if ($submit == "Jouer") {
            if (empty($joueur1) || empty($joueur2)) {
               $erreur = "Veuillez remplir les champs";
             }else {
                $_SESSION["joueur"]["nomJoueur1"] = $joueur1;
                $_SESSION["joueur"]["nomJoueur2"] = $joueur2;
                header('Location:game.php');
                exit;
            }
        }
    }

//print_r($_SESSION);
?>