<?php
require_once ('fonctionnement.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="./resources/css/<?= $_SESSION['couleur'] ?>">
    <title>Accueil</title>
</head>

<body>
    <header>
        <h2>Bienvenue sur la meilleure bataille navale du siècle</h2>
    </header>
    <form action="index.php" method="POST">
        <table border="1">
            <tr>
                <td>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Joueur 1</span>
                        </div>
                        <input type="text" name="joueur1" class="form-control" placeholder="Nom du joueur 1" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                </td>
                <td>
                    <h6>Choisir un avatar pour J1</h6>
                    <select name="avatarJ1">
                        <option <?php if($imageJ1=="1") echo 'selected="selected"'; ?> value="1">Avatar 1</option>
                        <option <?php if($imageJ1=="2") echo 'selected="selected"'; ?> value="2">Avatar 2</option>
                        <option <?php if($imageJ1=="3") echo 'selected="selected"'; ?> value="3">Avatar 3</option>
                    </select>
                    
                </td>
                <td>
                <img src=<?=$avatarJ1?>>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Joueur 2</span>
                        </div>
                        <input type="text" name="joueur2" class="form-control" placeholder="Nom du joueur 2" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                </td>
                <td>
                    <h6>Choisir un avatar pour J2</h6>
                    <select name="avatarJ2">
                        <option <?php if($imageJ2=="1") echo 'selected="selected"'; ?> value="1">Avatar 1</option>
                        <option <?php if($imageJ2=="2") echo 'selected="selected"'; ?> value="2">Avatar 2</option>
                        <option <?php if($imageJ2=="3") echo 'selected="selected"'; ?> value="3">Avatar 3</option>
                    </select>
                </td>
                <td>
                    <img src="<?=$avatarJ2?>">
                </td>
            </tr>
            
            <?php  
            if ($erreur != "") {
                echo "
                <tr>
                    <td colspan=2 class=\"options\">";
                        echo $erreur;
                        echo "<br>";
                   echo "</td>
                </tr>";
            }
            ?>
           
            <tr>
                <td class="options"><a href="parametres.php" class="btn btn-<?=$_SESSION['bouton']?>"> Paramètres </a></td>
                <td class="options"><input type="submit" name="btnSubmit" value="Jouer" class="btn btn-<?=$_SESSION['bouton']?>"></td>
                <td><button type="submit" name="btnActualiser" class="btn btn-<?=$_SESSION['bouton']?>">Actualiser</button></td>
            </tr>
        </table>
    </form>

   <footer>
        <div class="flex-container">
            <div>
                <b>Le but du jeu expliqué avec des mots simples </b><br>
                Informations : <br>
                - Vous jouez sur une même plateforme <br>
                - Chacun aura son tour de jeu <br>
                - Votre tour est limité par un timer <br>
                - Le joueur qui ne joue pas devra regarder ailleurs
            </div>
            <div>
                <b> Groupe composé de</b> <br>
                Adrien <br> Bernardo <br> Pablo <br> Thi-kim
            </div>
        </div>
    </footer>
</body>

</html>
